#!/usr/bin/env python3

import argparse
import logging
import pathlib
import sys

from fromager import overrides
from packaging.utils import parse_wheel_filename

from . import jobs, rpms

logger = logging.getLogger(__name__)

TERSE_LOG_FMT = '%(message)s'
VERBOSE_LOG_FMT = '%(levelname)s:%(name)s:%(lineno)d: %(message)s'


def main():
    parser = _get_argument_parser()
    args = parser.parse_args(sys.argv[1:])

    # Configure console and log output.
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG if args.verbose else logging.INFO)
    stream_formatter = logging.Formatter(VERBOSE_LOG_FMT if args.verbose else TERSE_LOG_FMT)
    stream_handler.setFormatter(stream_formatter)
    logging.getLogger().addHandler(stream_handler)
    if args.log_file:
        # Always log to the file at debug level
        file_handler = logging.FileHandler(args.log_file)
        file_handler.setLevel(logging.DEBUG)
        file_formatter = logging.Formatter(VERBOSE_LOG_FMT)
        file_handler.setFormatter(file_formatter)
        logging.getLogger().addHandler(file_handler)
    # We need to set the overall logger level to debug and allow the
    # handlers to filter messages at their own level.
    logging.getLogger().setLevel(logging.DEBUG)

    try:
        args.func(args)
    except Exception as err:
        logger.exception(err)
        raise


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False)
    parser.add_argument('--log-file', default='')

    subparsers = parser.add_subparsers(title='commands', dest='command')

    parser_pipeline_rules = subparsers.add_parser('pipeline-rules')
    parser_pipeline_rules.set_defaults(func=do_pipeline_rules)
    parser_pipeline_rules.add_argument('-w', '--wheels-repo')

    parser_find_rpms = subparsers.add_parser('find-rpms')
    parser_find_rpms.set_defaults(func=rpms.do_find_rpms)
    parser_find_rpms.add_argument('build_order_file', default='work-dir/build-order.json')
    parser_find_rpms.add_argument('--output', '-o')

    # The jobs CLI is complex enough that it's in its own module
    jobs.build_cli(parser, subparsers)

    return parser


def _get_requirements_from_args(args):
    to_build = []
    to_build.extend(args.toplevel)
    for filename in args.requirements_files:
        with open(filename, 'r') as f:
            for line in f:
                useful, _, _ = line.partition('#')
                useful = useful.strip()
                logger.debug('line %r useful %r', line, useful)
                if not useful:
                    continue
                to_build.append(useful)
    return to_build


def do_pipeline_rules(args):
    rule_template = '    - if: $CI_PIPELINE_SOURCE == "trigger" && $JOB == "build-wheel" && $DIST_NAME == "{dist_name}"'

    downloads_dir = pathlib.Path(args.wheels_repo) / 'downloads'

    dist_names = sorted(
        overrides.pkgname_to_override_module(parse_wheel_filename(filename.name)[0])
        for filename in sorted(downloads_dir.glob('*.whl'))
    )
    for dist_name in dist_names:
        print(rule_template.format(dist_name=dist_name))


if __name__ == '__main__':
    main()
